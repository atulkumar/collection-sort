package com.lara;
import java.util.ArrayList;
import java.util.Collections;

class D implements Comparable
{
	int i;
	D(int i)
	{
		this.i=i;
	}
	@Override
	public String toString()
	{
		return"i="+i;
	}
	@Override
	public int compareTo(Object o) 
	{
		return i-((D)o).i;
	}
}

public class M10
{
	public static void main(String[] args) 
	{
		ArrayList list=new ArrayList();
		list.add(new D(90));
		list.add(new D(190));
		list.add(new D(10));
		System.out.println(list);
		Collections.sort(list);
		System.out.println(list);
    }
}/*comparable interface implementing generics method
*sort() method internaly calling CompareTo()method.
*if elements are compareable type then only sort() other wise we will getting RTE.*/