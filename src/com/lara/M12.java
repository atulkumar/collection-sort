package com.lara;

import java.util.ArrayList;
import java.util.Collections;

public class M12
{
	public static void main(String[] args) 
	{
		ArrayList list=new ArrayList();
		list.add(new StringBuffer("hello"));
		list.add(new StringBuffer("abc"));
		list.add(new StringBuffer("xyz"));
		list.add(new StringBuffer("test"));
		list.add(new StringBuffer("java"));
		System.out.println(list);
		Collections.sort(list);
		System.out.println(list);
	}
}/*StringBuffer not implementing compareable type.
*we will be getting ClassCastException.*/