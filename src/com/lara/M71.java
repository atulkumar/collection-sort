package com.lara;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class M71
{
	public static void main(String[] args)
	{
		HashMap map=new HashMap();
		map.put("hello",100);
		map.put("abc",10);
		map.put("xyz",100);
		map.put("test",0);
		map.put("ram",150);
		map.put("java",200);
		map.put("check",120);
		map.put("lara",50);
		Set enteries=map.entrySet();
		Comparator c1=(o1,o2)->
		(Integer)((Map.Entry)o1).getValue()-(Integer)((Map.Entry)o2).getValue();
		TreeSet set=new TreeSet(c1);
		set.addAll(enteries);
		System.out.println(set);
	}
}/*TreeSet cotaining entry objects.values of entry object sorting.*/
