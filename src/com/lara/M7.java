package com.lara;

import java.util.ArrayList;

class A
{
	int i;
	A(int i)
	{
		this.i=i;
	}
}
public class M7
{
	public static void main(String[] args) 
	{
		ArrayList list=new ArrayList();
		list.add(new A(90));
		list.add(new A(190));
		list.add(new A(10));
		System.out.println(list);
    }
}/*every element is object of A.
*class A toString() method calling automaticaly.
*in every collections API toString method is override.*/