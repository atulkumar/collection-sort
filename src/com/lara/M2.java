package com.lara;

import java.util.ArrayList;
import java.util.Collections;

public class M2 
{
	public static void main(String[] args) 
	{
		ArrayList list=new ArrayList();
		list.add(90);
		list.add(9);
		list.add(0);
		list.add(190);
		list.add(910);
		list.add(290);
		System.out.println(list);
		Collections.sort(list,Collections.reverseOrder());
		System.out.println("sorted list in reverse order");
		System.out.println(list);
	}
}/*collections.reverseOrder() is printing to the descending order. */