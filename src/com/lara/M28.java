package com.lara;
import java.util.Comparator;
import java.util.PriorityQueue;
public class M28 
{
	static class A
	{
		int i;
		A(int i)
		{
			this.i=i;
		}
		@Override
		public String toString() 
		{
			return"i="+i;
		}
	}
	public static void main(String[] args) 
	{
		PriorityQueue queue=new PriorityQueue((o1,o2)->((A)o1).i-((A)o2).i);
		queue.add(new A(90));
		queue.add(new A(100));
		queue.add(new A(10));
		System.out.println(queue);
		System.out.println(queue.poll());
	  }
}