//lambda expressions
package com.lara;
import java.util.ArrayList;
import java.util.Collections;

class Y
{
	int i,j;
	Y(int i,int j)
	{
		this.i=i;
		this.j=j;
	}
	public String toString()
	{
		return "("+i+","+j+")";
	}
}
public class M16 

{
	public static void main(String[] args) 
	{
		ArrayList list=new ArrayList();
		list.add(new Y(10,20));
		list.add(new Y(20,15));
		list.add(new Y(15,10));
		list.add(new Y(1,200));
		list.add(new Y(18,0));
		System.out.println(list);
		Collections.sort(list,(o1,o2)->((Y)o1).i-((Y)o2).i);//it is the part of method body
		System.out.println(list);
		Collections.sort(list,(o1,o2)->((Y)o1).j-((Y)o2).j);
		System.out.println(list);
	}
}/*in case of lambda expression treated as a method.
*if it is only one method  emlpement.
*lambda expretions is only for single method emplementation.*/