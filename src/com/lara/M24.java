package com.lara;

import java.util.PriorityQueue;
public class M24 
{
	static class A
	{
		int i;
		A(int i)
		{
			this.i=i;
		}
		@Override
		public String toString() 
		{
			return"i="+i;
		}
	}
	public static void main(String[] args) 
	{
		PriorityQueue queue=new PriorityQueue();
		queue.add(new A(90));
		queue.add(new A(100));
		queue.add(new A(10));
		System.out.println(queue);
		System.out.println(queue.poll());
	}
}/*class A not a sorted type we will be getting ClassCastException.
if class is static we can't use starite away.*/