//syncronizing ArraList
package com.lara;

import java.util.ArrayList;

class SynchronizedArrayList extends ArrayList
{
	private ArrayList list;
	SynchronizedArrayList(ArrayList list)
	{
		this.list=list;
	}
	@Override
	public boolean add(Object e) 
	{
		boolean flag=false;
		synchronized(list)
		{
			flag=list.add(e);
		}
		return flag;
	}
	@Override
	public Object get(int index) 
	{
		Object obj=null;
		synchronized(list)
		{
			obj=list.get(index);
		}
		return obj;
	}@Override
	public Object set(int index, Object element) 
	{
		Object obj=null;
		synchronized(list)
		{
			obj=list.set(index, element);
		}
		return obj;
	}
}
public class M74
{
	public static void main(String[] args)
	{
		ArrayList list=new ArrayList();
		//section 1 *this section no-synchronized area,in this area data corruption
		list.add(90);
		list.add(90);
		
		list=new SynchronizedArrayList(list);
		//section 2
		list.add(190);
		list.add(10);
		list.add(390);
		list.add(490);
		System.out.println(list);
	}
}