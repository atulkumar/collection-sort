package com.lara;
import java.util.PriorityQueue;
public class M21
{
	public static void main(String[] args) 
	{
		PriorityQueue queue=new PriorityQueue();
		queue.add(90);
		queue.add(100);
		queue.add(10);
		queue.add(0);
		queue.add(400);
		queue.add(80);
		queue.add(20);
		System.out.println(queue);//sorted automatic from the list element
		System.out.println(queue.poll());
		System.out.println(queue);
		System.out.println(queue.poll());//always read & remove only fist element
		System.out.println(queue);
		System.out.println(queue.poll());
		System.out.println(queue);
	}
}/*Priority queue is a perfect queue type.
*it is auto sorted queue.*/