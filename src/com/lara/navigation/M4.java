package com.lara.navigation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
public class M4 
{
	public static void main(String[] args)
	{
		ArrayList l=new ArrayList();
		l.add("Z");
		l.add("A");
		l.add("K");
		l.add("L");
		System.out.println("Before Sorting:"+l);
		Collections.sort(l,new MyComparator());
		System.out.println("After Sorting:"+l);
	}
}
class  MyComparator implements Comparator
{
	public int compare(Object o1,Object o2)
	{
		String s1=(String)o1;
		String s2=o2.toString();
		return s2.compareTo(s1);
	}
}