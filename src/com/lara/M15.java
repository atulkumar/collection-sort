//annonimous inner class
package com.lara;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class X
{
	int i,j;
	X(int i,int j)
	{
		this.i=i;
		this.j=j;
	}
	public String toString() 
	{
		return "("+i+","+j+")";
	}
}
public class M15 
{
	public static void main(String[] args) 
	{
		ArrayList list=new ArrayList();
		list.add(new X(10,20));
		list.add(new X(20,15));
		list.add(new X(15,10));
		list.add(new X(1,200));
		list.add(new X(18,0));
		System.out.println(list);
		Collections.sort(list,new Comparator()
				{
			        public int compare(Object o1,Object o2)
			        {
					 return((X)o1).i-((X)o2).i;
			        }
				});
				System.out.println(list);
		Collections.sort(list,new Comparator()
				{
			        public int compare(Object o1,Object o2)
			        {
					 return((X)o1).j-((X)o2).j;
			        }
				});
		System.out.println(list);
	}
}/*if any method using only one fucntion then it is called functional expretions.
lambda Expressions:-*/