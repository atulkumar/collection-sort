package com.lara;

import java.util.PriorityQueue;

public class M23
{
	public static void main(String[] args) 
	{
		PriorityQueue queue=new PriorityQueue();
		queue.add(90);
		queue.add(100);
		queue.add(10);
		queue.add(0);
		queue.add(400);
		queue.add(80);
		queue.add(20);
		queue.add(null);//if adding null:- java.lang.NullPointerException
		System.out.println(queue);
		System.out.println(queue.poll());
	}
}