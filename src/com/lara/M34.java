package com.lara;

import java.util.HashSet;

public class M34
{
	public static void main(String[] args) 
	{
		class A
		{
			int i;
			A(int i)
			{
				this.i=i;
			}
			@Override
			public String toString() 
			{
				return "i="+i;
			}
			@Override
			public int hashCode() 
			{
				return Integer.toString(i).hashCode();
			}
			@Override
			public boolean equals(Object obj) 
			{
				return (obj instanceof A)&&(i==((A)obj).i);
			}
		}
		HashSet set=new HashSet();
		System.out.println(set.add(new A(90)));
		System.out.println(set.add(new A(90)));
		System.out.println(set.add(new A(90)));
		System.out.println(set.add(new A(90)));
		System.out.println(set.add(new A(90)));
		System.out.println(set);
	}
}/*in case of collection ware evere to avoid duplicate hashCode() & equals() method overrided.*/