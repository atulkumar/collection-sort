package com.lara;

import java.util.PriorityQueue;
public class M25
{
static class A implements Comparable
{
	int i;
	A(int i)
	{
		this.i=i;
	}
	@Override
	public String toString() 
	{
		return"i="+i;
	}
	@Override
	public int compareTo(Object o) 
	{
		return (i-((A)o).i);
	}
}
public static void main(String[] args) 
{
	PriorityQueue queue=new PriorityQueue();
	queue.add(new A(90));
	queue.add(new A(100));
	queue.add(new A(10));
	System.out.println(queue);
	System.out.println(queue.poll());
  }
}/*in priority queue fist one sorted.*/