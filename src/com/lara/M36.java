package com.lara;
import java.util.HashSet;
public class M36 
{
	static class A
	{
		int i,j,k;
		A(int i,int j,int k)
		{
			this.i=i;
			this.j=j;
			this.k=k;
		}
		@Override
		public String toString() 
		{
			return "("+i+","+j+","+k+")";
		}
		@Override
		public int hashCode() 
		{
			int hash=Integer.toString(i).hashCode();
			hash+=Integer.toString(j).hashCode();
			hash+=Integer.toString(k).hashCode();
			System.out.println("hashCode for:"+this+" is "+hash);
			return hash;
		}
		@Override
		public boolean equals(Object obj) 
		{
			boolean flag=(obj instanceof A)&&
					i==((A)obj).i &&
					j==((A)obj).j &&
					k==((A)obj).k;
			System.out.println("equals b/w"+this+"and"+obj+"falg");
			return flag;
		}
	}//end of A
	public static void main(String[] args) 
	{
		HashSet set=new HashSet();
		set.add(new A(10,20,30));
		System.out.println("-----------------------");
		set.add(new A(10,20,30));
		System.out.println("-----------------------");
		set.add(new A(20,10,30));
		System.out.println("-----------------------");
		set.add(new A(20,10,30));
		System.out.println("-----------------------");
		set.add(new A(30,20,10));
		System.out.println("-----------------------");
		set.add(new A(30,20,10));
		System.out.println("-----------------------");
		set.add(new A(10,10,10));
		System.out.println("-----------------------");
	}
}/*while we are adding any element of the hashcode().
*while storing 1st element in calling hashcode() choosing a index.
*but we are adding second element fist compareing fist element.
*second element coparing fiat element same are not.
*now adding 3rd element comparing previous element.
*allready the fist element has same are not then storing inside hashcode().
*3rd element calling equals().
*3rd elenemt comparing every element.
*because of same hash code then storing same bucket.
*3rd element adding in to 1st bucket and 1st element refer to 3rd element.
*then 3rd element got added.
*when ever we are calling different element then different has coe generating ,
 after that new bucket will be storing.
 if we are adding different value then only hascode() is calling*/
