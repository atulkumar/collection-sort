package com.lara.pack2;

import java.util.HashMap;

public class M30
{
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) 
	{
		HashMap map = new HashMap();
		map.put(null, null);
		map.put("key1", 10);
		map.put("key2", 20);
		map.put("key3", 30);
		map.put("key4", 40);
		map.put("key5", 50);
		//Set keys = map.keySet();    // unique values usage; no order of series
		//TreeMap map1 = new TreeMap();  // no null; 
		
		//LinkedHashMap map2 = new LinkedHashMap(map);
		
		System.out.println(map);
		//System.out.println(keys);
		//System.out.println(map1);	
	}
}