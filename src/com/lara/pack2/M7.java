package com.lara.pack2;

import java.util.PriorityQueue;

public class M7
{
	static class A implements Comparable
	{
		int i;
		A(int i)
		{
			this.i = i;
		}
		
		@Override
		public String toString()
		{
			return " i = " + i ;
		}
		
		@Override
		public int compareTo(Object o)
		{
			return (i - ((A)o).i) ;
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) 
	{
		PriorityQueue queue = new PriorityQueue();
		queue.add(new A(90));
		queue.add(new A(100));
		queue.add(new A(80));
		queue.add(new A(30));
		
		System.out.println(queue);
		System.out.println(queue.poll());
		
	}
}/*
OUTPUT:-
RTE:- java.lang.ClassCastException: pack2.M6$A cannot be cast to java.lang.Comparable 
*/
