package com.lara.pack2;

import java.util.Hashtable;

public class M25
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) throws InterruptedException 
      {
		Hashtable tab = new Hashtable();           // Synchronised ; null not allowed;
		
        //System.out.println(tab.put(null, null));entry = key , value ; object type ; key != duplicates , values == duplicates
		System.out.println(tab.put("2", 200));
		System.out.println(tab.put("3", 200));  // duplicates not returning unique value; only unique keys return null
		System.out.println(tab.put("1", 300));  // duplicates not returning unique value; only unique keys return null
		System.out.println(tab.put("4", 400));  // duplicates not returning unique value; only unique keys return null
		System.out.println(tab.put(true , 100));
		System.out.println(tab.put(4.5 , 700));
		
		
		System.out.println(tab);
		
		
      }
}
