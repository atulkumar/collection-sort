package com.lara.pack2;

import java.util.TreeSet;

public class M18
{
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args)
	{
		TreeSet set = new TreeSet();   // directly use it when you want ordering of the nos // auto-sorting
		set.add(90);
		set.add(9);
		set.add(0);
		set.add(190);
		set.add(910);
		set.add(10);
		set.add(940);
		set.add(91);
		
		System.out.println(set);
		
	}
}
