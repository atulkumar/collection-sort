package com.lara.pack2;

import java.util.LinkedList;

public class M3 
{	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) 
    {
		LinkedList list = new LinkedList();
		list.add(90);
		list.add(100);
		list.add("abc");
		list.add("hello");
		System.out.println(list);
		
		System.out.println(list.poll());                    //  removing ; just peeking
		System.out.println(list);
	}
}
