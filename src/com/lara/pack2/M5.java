package com.lara.pack2;

import java.util.PriorityQueue;

public class M5 
{
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) 
	{
		PriorityQueue queue = new PriorityQueue();  
		// ** auto-sorted queue for first element only **   // perfect queue   // removes only head/top/first element
		                                       
		queue.add(90);
		queue.add(100);
		queue.add(10);
		queue.add(0);
		queue.add(400);
		queue.add(8);
		queue.add(20);
		// queue.add(0.0);   RTE : ClassCastException
		queue.add(null);    // not recommended  
		
		System.out.println(queue);
		
		System.out.println(queue.poll());
		System.out.println(queue);
		
	}
}/*
OUTPUT: -
RTE ---->     java.lang.NullPointerException
*/

