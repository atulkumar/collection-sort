package com.lara.pack2;

import java.util.HashSet;

public class M13
{
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) 
    {
		class A
		{
			int i;
			A(int i)
			{
				this.i = i;
			}
			
			@Override
			public String toString()
			{
				return " i = " + i;
			}
		}
		
		HashSet set = new HashSet();         
   	System.out.println(set.add(new A(90)));
   	System.out.println(set.add(new A(90)));  
   	System.out.println(set.add(new A(90)));
   	System.out.println(set.add(new A(90)));
   	System.out.println(set.add(new A(90)));
   	System.out.println(set.add(new A(90)));
   	
   	System.out.println(set);     // hashCode() & equals() are not overrided in A(90)/StringBuilder; duplicates allowed
		
	 }
}
