package com.lara.pack2;

import java.util.HashMap;

public class M22
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) 
      {
		HashMap map = new HashMap();
		System.out.println(map.put("abc", 100));  // entry = key , value ; object type ; key != duplicates , values == duplicates
		System.out.println(map.put("key1", 200));
		System.out.println(map.put("key2", 300));
		System.out.println(map.put("key3", 400));
		
		System.out.println(map);
      }
}
