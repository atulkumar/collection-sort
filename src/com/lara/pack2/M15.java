package com.lara.pack2;

import java.util.HashSet;

public class M15 
{

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) 
    {
		class A         // local inner class ; hashCode() overrided
		{
			int i;
			A(int i)
			{
				this.i = i;
			}
			
			@Override
			public String toString()
			{
				return " i = " + i;
			}
			
			@Override
			public int hashCode()
			{
				return Integer.toString(i).hashCode();
			}
			

			@Override
			public boolean equals(Object obj)
			{
				return (obj instanceof A) && (i == ((A)obj).i);
			}

		}
		
		HashSet set = new HashSet();         
   	System.out.println(set.add(new A(90)));
   	System.out.println(set.add(new A(90)));  
   	System.out.println(set.add(new A(90)));
   	System.out.println(set.add(new A(90)));
   	System.out.println(set.add(new A(90)));
   	System.out.println(set.add(new A(90)));
   	
   	System.out.println(set);     // hashCode() & equals() are overrided in A(90); no duplicates allowed
		
	 }
}
