package com.lara.pack2;

import java.util.HashMap;
import java.util.TreeMap;

public class M27
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) 
      {
		HashMap map = new HashMap();           // null allowed;
		
        //map.put(null, null); entry = key , value ; object type ; key != duplicates , values == duplicates
		map.put("2", 200);
		map.put("3", 200);  
		map.put("1", 300);  
		map.put("4", 400);  
		map.put("key2" , 100);
		map.put("key3" , 700);
		
		System.out.println(map);
		
		TreeMap map1 = new TreeMap(map);       // not allowing null keys; ordered; takes reference of HashMap "keys" = comparable type
		                                       // else supply comparator type
		//map1.put(null, null);                // null keys ;not allowed
		//map1.put("abc", 1000);
		//map1.put("xyz", 2000);
		
		System.out.println(map1);             // sorted on key basis
		

      }
}
