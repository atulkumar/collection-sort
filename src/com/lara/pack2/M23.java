package com.lara.pack2;

import java.util.HashMap;

public class M23
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) 
      {
		HashMap map = new HashMap();
		System.out.println();
		System.out.println(map.put("abc", 100));  // entry = key , value ; object type ; key != duplicates , values == duplicates
		System.out.println(map.put("abc", 200));  // duplicates not returning unique value; only unique keys return null
		System.out.println(map.put("abc", 300));  // duplicates not returning unique value; only unique keys return null
		System.out.println(map.put("abc", 400));  // duplicates not returning unique value; only unique keys return null
		
		System.out.println(map);
      }
}
