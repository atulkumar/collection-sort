package com.lara.pack2;

import java.util.LinkedHashMap;

public class M26 
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) throws InterruptedException 
      {
		LinkedHashMap map = new LinkedHashMap();           // null allowed;
		
        System.out.println(map.put(null, null)); //entry = key , value ; object type ; key != duplicates , values == duplicates
		System.out.println(map.put("2", 200));
		System.out.println(map.put("3", 200));  
		System.out.println(map.put("1", 300));  
		System.out.println(map.put("4", 400));  
		System.out.println(map.put(true , 100));
		System.out.println(map.put(4.5 , 700));
		
		System.out.println(map);

      }
}
