package com.lara.pack2;

import java.util.Comparator;
import java.util.TreeSet;

public class M21
{
	static class A
	{
		int i , j;
		A(int i, int j)
		{
			this.i = i;
			this.j = j;
		}
		
		@Override
		public String toString()
		{
			return "(" + i +  " , " + j + ")";
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args)
	{
		TreeSet set = new TreeSet(new Comparator()
		{
		  @Override
		  public int compare(Object o1 , Object o2 )
		  {
			  return ((A)o1).i - ((A)o2).i;
		  }
		}); // inner class
	
		set.add(new A(10 , 20));
		set.add(new A(0 , 200));
		set.add(new A(100 , 0));
		set.add(new A(50 , 2000));
		
		System.out.println(set);
	}
}
