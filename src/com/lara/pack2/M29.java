package com.lara.pack2;
import java.util.TreeMap;
class A
{
	int i , j;
	A(int i , int j)
	{
		this.i = i;
		this.j = j;
	}
	
	@Override
	public String toString()
	{
		return"(" + i + " , " + j + ")";
	}
}
public class M29 
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) 
      {
		TreeMap map = new TreeMap((o1 , o2) -> ((A)o1).i - ((A)o2).j);
		// map.put(null,null);  not allowed
		map.put("2", 200);
		map.put("3", 200);  
		map.put("1", 300);  
		map.put("4", 400);  
		map.put("key2" , 100);
		map.put("key3" , 700);
		
		System.out.println(map);             // sorted on key basis	

      }
}
/*OUTPUT:-

{1=300, key2=100, 2=200, 3=200, 4=400, key3=700} // user - order

*/