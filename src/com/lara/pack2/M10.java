package com.lara.pack2;

import java.util.PriorityQueue;

public class M10
{
	static class A 
	{
		int i;
		A(int i)
		{
			this.i = i;
		}
		
		@Override
		public String toString()
		{
			return " i = " + i ;
		}
		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	
	public static void main(String[] args) 
	{
		PriorityQueue queue = new PriorityQueue((o1 , o2) -> ((A)o1).i - ((A)o2).i);        // lambda expression
		queue.add(new A(90));
		queue.add(new A(100));
		queue.add(new A(80));
		queue.add(new A(30));
		
		System.out.println(queue);
		System.out.println(queue.poll());
		
	}
}/*
OUTPUT:-
[ i = 30,  i = 80,  i = 90,  i = 100]
i = 30

*/
