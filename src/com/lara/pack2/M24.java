package com.lara.pack2;

import java.util.HashMap;

public class M24 
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) throws InterruptedException 
      {
		HashMap map = new HashMap();         // Non-Synchronised
		
		System.out.println(map.put(null, null));  // entry = key , value ; object type ; key != duplicates , values == duplicates
		System.out.println(map.put("2", 200));
		System.out.println(map.put("3", 200));  // duplicates not returning unique value; only unique keys return null
		System.out.println(map.put("1", 300));  // duplicates not returning unique value; only unique keys return null
		System.out.println(map.put("4", 400));  // duplicates not returning unique value; only unique keys return null
		System.out.println(map.put(true , 100));
		System.out.println(map.put(4.5 , 700));
		
		System.out.println("=====");
		
		System.out.println(map.get("1"));
		System.out.println(map.get(true));
		System.out.println(map.get(4.5));
		
		System.out.println(map);
		
		
      }
}
