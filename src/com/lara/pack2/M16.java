package com.lara.pack2;

import java.util.HashSet;

public class M16 
{
	static class A
    {
        int i , j , k;
        A(int i , int j, int k)
       {
            this.i = i;
            this.j = j;
            this.k = k;
       }
    
    @Override
    public String toString()
   {
     return "(" + i + " , " + j + " , " + k + ")";
   }

    @Override
    public int hashCode()
   {
      int hash = Integer.toString(i).hashCode();
      hash += Integer.toString(j).hashCode();
      hash += Integer.toString(k).hashCode();
      System.out.println("hashCode for  " + this + " is " + hash);
      return hash;  
    }
  
    @Override
    public boolean equals(Object obj)
    {
    	boolean flag = (obj instanceof A) && 
    			i == ((A)obj).i && 
    			j == ((A)obj).j && 
    			k == ((A)obj).k ;
    	
    	System.out.println("equals b/w " + this + " and " + obj + " is " + flag);
    	return flag;
    }
    
  }// end of A
      
      @SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) 
      {
		   HashSet set = new HashSet(); 
 /* calling hashCode() for first element ; particular hash no that no is stored 
    for every hash bucket is labelled as a hash no. ; first element is stored | while storing second element it is again calling 
    hashCode() getting hash no; it is comparing the first element with second element for duplication/not 
	and checking if that no is assigned to a bucket or not; returning false/true stored in the same bucket or a new bucket respectively. */
		   
		   set.add(new A(10 , 20 , 30));                  // (1.)  == (2.)
		   System.out.println("------------");      
		   
		   set.add(new A(10 , 20 , 30));                  // (2.) // not storing
		   System.out.println("------------");
		   
		   set.add(new A(20 , 10 , 30));                  // (3.) !== (1.)  |  == (4.) 
		   System.out.println("------------");
		   
		   set.add(new A(20 , 10 , 30));                  // (4.) == (3.) same elements = true
		   System.out.println("------------");
		   
		   set.add(new A(30 , 20 , 10));                  // (5.)   !== (1) && !== (3.) = false
		   System.out.println("------------");
		   
		   set.add(new A(30 , 20 , 10));                 // (6.) !== (1.) !== (3.) == (5.) = true not storing
		   System.out.println("------------");
		   
		   System.out.println(" ___________");
		   System.out.println("|           |");
		   System.out.println("|   BREAK   |");
		   System.out.println("|           |");
		   System.out.println("|___________| ");
		   
		   set.add(new A(10 , 10 , 10));                 // (6.) !== (1.) !== (3.) == (5.) = true not storing
		   System.out.println("------------");
		   
		   set.add(new A(30 , 10 , 10));                 // (6.) !== (1.) !== (3.) == (5.) = true not storing
		   System.out.println("------------");

		   
		   set.add(new A(10 , 30 , 10));                 // (6.) !== (1.) !== (3.) == (5.) = true not storing
		   System.out.println("------------");

		   
		   set.add(new A(10 , 10 , 30));                 // (6.) !== (1.) !== (3.) == (5.) = true not storing
		   System.out.println("------------");

		   	   
	  }
}
