package com.lara.pack2;

import java.util.Comparator;
import java.util.PriorityQueue;

public class M8
{
	static class A 
	{
		int i;
		A(int i)
		{
			this.i = i;
		}
		
		@Override
		public String toString()
		{
			return " i = " + i ;
		}
		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	
	public static void main(String[] args) 
	{
		Comparator c1 = new Comparator()
	  {
		public int compare (Object o1 , Object o2)
		{
			return ((A)o1).i - ((A)o2).i;
		};
	  };
		PriorityQueue queue = new PriorityQueue();
		queue.add(new A(90));
		queue.add(new A(100));
		queue.add(new A(80));
		queue.add(new A(30));
		
		System.out.println(queue);
		System.out.println(queue.poll());
		
	}
}/*
OUTPUT:-
RTE:- java.lang.ClassCastException: pack2.M6$A cannot be cast to java.lang.Comparable 
*/
