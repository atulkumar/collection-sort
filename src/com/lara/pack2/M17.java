package com.lara.pack2;

import java.util.HashSet;
import java.util.TreeSet;

public class M17
{
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args)
	{
		HashSet set = new HashSet();
		set.add(90);
		set.add(9);
		set.add(0);
		set.add(190);
		set.add(910);
		set.add(10);
		set.add(940);
		set.add(91);
		
		System.out.println(set);
		TreeSet set1 = new TreeSet(set);
		System.out.println(set1);               // auto-ordering, no null;
	}

}
