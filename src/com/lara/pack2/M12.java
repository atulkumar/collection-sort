package com.lara.pack2;

import java.util.HashSet;

public class M12 
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args)
    {
    	HashSet set = new HashSet();         
    	System.out.println(set.add(new StringBuffer("abc")));
    	System.out.println(set.add(new StringBuffer("abc")));  
    	System.out.println(set.add(new StringBuffer("abc")));
    	System.out.println(set.add(new StringBuffer("abc")));
    	System.out.println(set.add(new StringBuffer("abc")));
    	System.out.println(set.add(new StringBuffer("abc")));
    	
    	System.out.println(set);     // hashCode() & equals() are not overrided in StringBuffer/StringBuilder; duplicates allowed
		
	}
}
