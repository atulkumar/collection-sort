package com.lara.pack2;

import java.util.TreeMap;

public class M28
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) 
      {
		TreeMap map = new TreeMap();
		// map.put(null,null);  not allowed
		map.put("2", 200);
		map.put("3", 200);  
		map.put("1", 300);  
		map.put("4", 400);  
		map.put("key2" , 100);
		map.put("key3" , 700);

		
		System.out.println(map);             // sorted on key basis
		

      }

}
