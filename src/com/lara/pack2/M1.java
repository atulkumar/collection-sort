package com.lara.pack2;

import java.util.LinkedList;

public class M1
{
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) 
    {
		LinkedList list = new LinkedList();
		list.add(90);
		list.add(100);
		list.add("abc");
		list.add("hello");
		System.out.println(list);
		
		System.out.println(list.removeFirst());
		System.out.println(list);
	}
}
