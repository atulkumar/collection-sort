package com.lara.pack2;

import java.util.HashSet;

public class M11 
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args)
    {
    	HashSet set = new HashSet();         
    	System.out.println(set.add(90));    // first time - true
    	
    	System.out.println(set.add(90));    // duplicates 
    	System.out.println(set.add(90));    // duplicates 
    	System.out.println(set.add(90));    // duplicates 
    	System.out.println(set.add(90));    // duplicates 
    	
    	System.out.println(set);            // hashCode() & equals() are overrided in wrapper class; no duplicates allowed
		
	}
}
