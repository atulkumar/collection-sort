package com.lara;

import java.util.HashSet;

public class M33
{
	public static void main(String[] args) 
	{
		class A
		{
			int i;
			A(int i)
			{
				this.i=i;
			}
			@Override
			public String toString() 
			{
				return "i="+i;
			}
			@Override
			public int hashCode() 
			{
				return Integer.toString(i).hashCode();
			}
		}
		HashSet set=new HashSet();
		System.out.println(set.add(new A(90)));
		System.out.println(set.add(new A(90)));
		System.out.println(set.add(new A(90)));
		System.out.println(set.add(new A(90)));
		System.out.println(set.add(new A(90)));
		System.out.println(set);
	}
}//only hashCode() method are overrided.