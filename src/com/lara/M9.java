package com.lara;

import java.util.ArrayList;
import java.util.Collections;

class C
{
	int i;
	C(int i)
	{
		this.i=i;
	}
	@Override
	public String toString()
	{
		return"i="+i;
	}
}

public class M9
{
	public static void main(String[] args) 
	{
		ArrayList list=new ArrayList();
		list.add(new C(90));
		list.add(new C(190));
		list.add(new C(10));
		System.out.println(list);
		Collections.sort(list);
		System.out.println(list);
    }
}/*Cnobject are storing wich ever collections sort.
*sort method putting one condition then only containing.
*if we used to sort method element shoud have coparable type.
*every String and wrapper class implementing to the compable type*/