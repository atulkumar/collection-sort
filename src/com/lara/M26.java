package com.lara;

import java.util.Comparator;
import java.util.PriorityQueue;
public class M26 
{
	static class A
	{
		int i;
		A(int i)
		{
			this.i=i;
		}
		@Override
		public String toString() 
		{
			return"i="+i;
		}
	}
	public static void main(String[] args) 
	{
		Comparator c1=new Comparator()
				{
			      public int compare(Object o1,Object o2) 
			         {
                      return ((A)o1).i-((A)o2).i;
				    };
				};
		PriorityQueue queue=new PriorityQueue(c1);
		queue.add(new A(90));
		queue.add(new A(100));
		queue.add(new A(10));
		System.out.println(queue);
		System.out.println(queue.poll());
	  }
}/*if*/