package com.lara;

import java.util.HashSet;

public class M32
{
	public static void main(String[] args) 
	{
		class A
		{
			int i;
			A(int i)
			{
				this.i=i;
			}
			@Override
			public String toString() 
			{
				return "i="+i;
			}
		}
		HashSet set=new HashSet();
		System.out.println(set.add(new A(90)));
		System.out.println(set.add(new A(90)));
		System.out.println(set.add(new A(90)));
		System.out.println(set.add(new A(90)));
		System.out.println(set.add(new A(90)));
		System.out.println(set);
	}
}/*in the class A hashCode() & equals() both are not overrided.*/