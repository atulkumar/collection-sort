package com.lara;

import java.util.ArrayList;

class B
{
	int i;
	B(int i)
	{
		this.i=i;
	}
	@Override
	public String toString()
	{
		return"i="+i;
	}
}
public class M8 
{
	public static void main(String[] args) 
	{
		ArrayList list=new ArrayList();
		list.add(new B(90));
		list.add(new B(190));
		list.add(new B(10));
		System.out.println(list);
    }
}