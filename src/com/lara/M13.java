package com.lara;
import java.util.ArrayList;
import java.util.Collections;
class E implements Comparable
{
	int i,j;
	E(int i,int j)
	{
		this.i=i;
		this.j=j;
	}
	@Override
	public String toString() 
	{
		return"(i="+i+",j="+j+")";
	}
	@Override
	public int compareTo(Object o) 
	{
		return i-((E)o).i;
	}
}
public class M13
{
	public static void main(String[] args)
	{
		ArrayList list=new ArrayList();
		list.add(new E(10,20));
		list.add(new E(0,40));
		list.add(new E(100,0));
		list.add(new E(50,30));
		list.add(new E(6,10));
		System.out.println(list);
		Collections.sort(list);
		System.out.println(list);
	}
}/*corrent obj to copare other obj.
*collections sort method it is requared cotainer.
*comparable interface used only for if has single class using.
*compareTo() used single element compare,specify one attribute compare.*/