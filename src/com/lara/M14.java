package com.lara;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class F
{
	int i,j;
	F(int i,int j)
	{
		this.i=i;
		this.j=j;
	}
	@Override
	public String toString() 
	{
	return "("+i+","+j+")";
	}
}
class G implements Comparator
{
	@Override
	public int compare(Object o1, Object o2) 
	{
		return ((F)o1).i-((F)o2).i;
	}
}
class H implements Comparator
{
	@Override
	public int compare(Object o1, Object o2) 
	{
		return ((F)o1).j-((F)o2).j;
	}
}
public class M14 
{
	public static void main(String[] args) 
	{
		ArrayList list=new ArrayList();
		list.add(new F(10,20));
		list.add(new F(1,50));
		list.add(new F(30,30));
		list.add(new F(50,10));
		list.add(new F(10,20));
		list.add(new F(20,40));
		System.out.println(list);//no sorted list
		Collections.sort(list,new G());
		System.out.println(list);//sort by'i'
		
		Collections.sort(list,new H());
		System.out.println(list);//sorted by'j'
	}
}/*A seperate class G implements comparatore interface.
*comparatore() available in 'java.util' package.
*data type is 2nd to the comparable type.
*every time compareTo()method taking 2 object.*/