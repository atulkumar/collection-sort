package com.lara.pack3;

import java.util.Iterator;
import java.util.Vector;

public class M4 
{
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) 
	{
		Vector v = new Vector();
		v.add(90);
		v.add(910);
		v.add(99);
		v.add(true);
		v.add(false);
		v.add(null);
		v.add(100);
		Iterator i = v.iterator();    // Iterator = interface; read elements one by one; only one time
      while(i.hasNext())      // 1st method of Iterator
		    {
			     System.out.println(i.next());   // 2nd method of Iterator
		    }  // not reading second time
              Iterator i1 = v.iterator(); // Iterator = interface; read elements one by one; only one time
      while(i1.hasNext())      // 1st method of Iterator
		    {
			     System.out.println(i1.next());   // 2nd method of Iterator
		    }
	}
}