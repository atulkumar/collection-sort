package com.lara.pack3;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class M14 
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args)
	{
		HashMap map = new HashMap();   // Multiple entry objects into the Set
		map.put("abc", 90);
		map.put("xyz", 10);
		map.put("test", 910);
		map.put("test1", 100);
		map.put("test2", 50);
		
		Set set = map.entrySet();  // entrySet() method - reading every new object stored into Set
		Iterator it = set.iterator(); // getting iterator for the above set; where all entries are retrieved into sets having key & value
		Entry entry = null; 	
		while(it.hasNext())
		{
			entry = (Entry) it.next();
			System.out.println(entry.getKey() + " : " + entry.getValue());
		}
	}
}