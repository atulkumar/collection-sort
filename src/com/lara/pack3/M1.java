package com.lara.pack3;

import java.util.Enumeration;
import java.util.Vector;

public class M1 
{
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) 
	{
		Vector v = new Vector(); // Container = Iterator points to it
		v.add(90);
		v.add(910);
		v.add(99);
		v.add(true);
		v.add(false);
		v.add(null);
		v.add(100);
		
		Enumeration elements = v.elements();// Enumeration = interface; read elements one by one
		while(elements.hasMoreElements())// 1st method of Enumeration
		{
			System.out.println(elements.nextElement());
			
		}
	}
}