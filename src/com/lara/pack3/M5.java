package com.lara.pack3;

import java.util.Iterator;
import java.util.Vector;

public class M5
{
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) 
	{
		Vector v = new Vector();// Container = Iterator points to it; removing elements from Iterator points [Vector :- removal from this only]
		v.add(90);
		v.add(910);
		v.add(99);
		v.add(true);
		v.add(false);
		v.add(null);
		v.add(100);
	 Iterator i = v.iterator(); // Iterator = interface; read elements one by one/remove/next; only one time; pointer to Vector
	      while(i.hasNext())      // 1st method of Iterator
		    {
			     System.out.println(i.next());   // 2nd method of Iterator
			     i.remove();
			     System.out.println(v);
		    }
	}
}