package com.lara.pack3;
import java.util.ListIterator;
import java.util.Vector;

public class M9
{
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) 
	{
		Vector v = new Vector();   // Container = Iterator points to it; removing elements from Iterator points [Vector :- removal from this only]
		v.add(90);
		v.add(910);
		v.add(99);
		v.add(true);
		v.add(false);
		
     ListIterator i = v.listIterator(); // ListIterator = interface; extends Iterator; same functions with new modifications
                                        // multiple times reading elements possible
              
		      while(i.hasNext())      // 1st method of Iterator
		    {
			     System.out.println(i.next());
		    }
		      System.out.println("======");
		      while(i.hasPrevious())      // 1st method of Iterator
			    {
				     System.out.println(i.previous());				
			    }
		      System.out.println(v);	   
	}
}