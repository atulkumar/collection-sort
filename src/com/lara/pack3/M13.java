package com.lara.pack3;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;

public class M13 
{
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) 
	{
		HashSet set = new HashSet();   // Container = Iterator points to it; removing elements from Iterator points [Vector :- removal from this only]
		set.add(90);
		set.add(910);
		set.add(99);
		set.add(true);
		set.add(false);
	/*	ListIterator i = (ListIterator) set.iterator(); // ListIterator = interface; extends Iterator; same functions with new modifications
                                      // multiple times reading elements possible
            Object i1;
		      while(i.hasNext())      // 1st method of Iterator
		    {
		    	  i1 = i.next();
			     System.out.println(i1);
			   
			     if(i1.equals(true))    
			     {
			    	((HashSet) i).add(500);
			     }
		    }*/
		      System.out.println("======");
		      System.out.println(set);           // after true we can add(500) after it
	}
}