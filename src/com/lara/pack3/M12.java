package com.lara.pack3;
import java.util.ListIterator;
import java.util.Vector;

public class M12 
{
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) 
	{
		Vector v = new Vector();   // Container = Iterator points to it; removing elements from Iterator points [Vector :- removal from this only]
		v.add(90);
		v.add(910);
		v.add(99);
		v.add(true);
		v.add(false);
		
     ListIterator i = v.listIterator(); // ListIterator = interface; extends Iterator; same functions with new modifications
                                        // multiple times reading elements possible
              Object i1;
		      while(i.hasNext())      // 1st method of Iterator
		    {
		    	  i1 = i.next();
			     System.out.println(i1);
			   
			     if(i1.equals(true))    
			     {
			    	i.add(500);
			     }
		    }
		      System.out.println("======");
		      System.out.println(v);           // after true we can add(500) after it	   
	}
}