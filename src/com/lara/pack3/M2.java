package com.lara.pack3;

import java.util.Enumeration;
import java.util.Vector;

public class M2 
{
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) 
	{
		Vector v = new Vector();
		v.add(90);
		v.add(910);
		v.add(99);
		v.add(true);
		v.add(false);
		v.add(null);
		v.add(100);
		
	 Enumeration elements = v.elements();// Enumeration = interface; read elements one by one; only one time
	 // Vector = Container => Enumeration points to it; next elements from Enumeration points [Vector :- next from this only]
 	      while(elements.hasMoreElements())// 1st method of Enumeration
		    {
			     System.out.println(elements.nextElement());//2nd method of Enumeration
		    }//not reading second time
        while(elements.hasMoreElements())// 1st method of Enumeration
		    {
			     System.out.println(elements.nextElement());
		    }
	}
}